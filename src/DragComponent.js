import DragEmitter from "./DragEmitter";

export default {
  name: "VListDraggable",

  props: {
    value: {
      type: Array,
      default: []
    }
  },

  data() {
    return {
      uniqueId: null,

      dragElem: {
        link: null,
        cloneLink: null,
        offset: {
          x: 0, y: 0
        },
        mouseOffset: {
          x: 0, y: 0
        }
      },
      dragElemClone: {
        link: null,
        offset: {
          x: 0, y: 0
        },
        mouseOffset: {
          x: 0, y: 0
        }
      }
    };
  },

  watch: {
    value: {
      handler() {
        this.$nextTick(function () {
          this.watchChildren();
        });
      },
      deep: true
    }
  },

  methods: {
    resetDragElem() {
      this.dragElem.link.removeAttribute("style");

      this.dragElem.link = null;
      this.dragElem.offset.x = 0;
      this.dragElem.offset.y = 0;
    },

    addItem(data) {
      let newArr = [].concat(this.value);
      newArr.push(data);

      this.$emit("input", newArr);
    },

    removeItem(index) {
      let newArr = [].concat(this.value);
      newArr.splice(index, 1);

      this.$emit("input", newArr);
    },

    findParentFromChildByClass(element, classname) {
      if (!element) {
        return null;
      }

      if (element.classList && element.classList.contains(classname)) {
        return element;
      } else if (element.parentNode) {
        return this.findParentFromChildByClass(element.parentNode, classname);
      } else {
        return null;
      }
    },

    watchChildren() {
      this.$el.childNodes.forEach((draggItem) => {
        if (draggItem.classList.contains("dragg-elem")) {
          return null;
        }

        draggItem.classList.add("dragg-elem");

        const mouseMove = (event) => {
          //console.log(event);
          this.posDraggElem = event;
        };

        const mouseUp = (event) => {
          this.dragElem.link.classList.remove("ghost");
          this.dragElem.link.style.display = "none";

          document.removeEventListener("mousemove", mouseMove);
          document.removeEventListener("mouseup", mouseUp);

          const draggComponent = this.findParentFromChildByClass(
              document.elementFromPoint(event.clientX, event.clientY),
              "v-list-draggable"
          );

          if (draggComponent) {
            const draggId = draggComponent.getAttribute("data-v-dragg");

            // Dragg end on parent component
            if (draggId != this.uniqueId) {
              const index = this.dragElem.link.getAttribute("data-key");
              DragEmitter.emit(draggId + ":move", this.value[index]);

              this.removeItem(index);
            }
          }
          this.dragElem.link.remove();
          this.resetDragElem();
        };
        draggItem.addEventListener('mousedown', (event) => {

          this.dragElem.cloneLink = draggItem;
          this.dragElem.link = draggItem.cloneNode(true);
          if (document.getElementsByClassName('panels-container')[0])
            document.getElementsByClassName('panels-container')[0].appendChild(this.dragElem.link);
          else
            document.body.appendChild(this.dragElem.link);
          // console.log("ClickEvent: ", event.x, event.y);
          // console.log("RectPosition: ", draggItem.getBoundingClientRect());
          let extData = draggItem.getBoundingClientRect();

          this.dragElem.offset.x = (extData.x - event.x) - 12;
          this.dragElem.offset.y = (extData.y - event.y) + (extData.height / 2);
          // console.log("offsets: ", this.dragElem.offset.x, this.dragElem.offset.y)
          this.dragElem.link.style.position = "fixed";

          document.addEventListener("mousemove", mouseMove);
          document.addEventListener("mouseup", mouseUp);

          this.posDraggElem = event;
          this.dragElem.link.classList.add("ghost");
        }, true);
      });
    }
  },

  mounted() {
    this.uniqueId = Math.random().toString(36).substr(2, 16);

    this.watchChildren();

    DragEmitter.subscribe(`${this.uniqueId}:move`, (data) => {
      this.addItem(data);
    });
  },

  computed: {
    posDraggElem: {
      get() {
        return {
          x: this.dragElem.link.offsetLeft,
          y: this.dragElem.link.offsetTop
        };
      },

      set(event) {
        this.dragElem.link.style.left = event.x + this.dragElem.offset.x + "px";
        this.dragElem.link.style.top = event.y + this.dragElem.offset.y + "px"
      }
    }
  },

  template: `
    <div class="v-list-draggable" :data-v-dragg="uniqueId">
      <slot></slot>
    </div>
  `
};
